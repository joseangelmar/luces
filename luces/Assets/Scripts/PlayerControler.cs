using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    public float maxSpeed = 5f;
    public float speed = 15f;
    private Rigidbody2D rb2d;
     public float jumpForce = 5f;
     public bool checktground;
    public GameObject linterna;
    public bool linternaEn;
    private void Awake()
    {
        linternaEn = false;
        linterna.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        if ((h == 0) && (checktground==true))
        {
            rb2d.velocity = Vector2.zero;
        }
        else
        {
            rb2d.AddForce(Vector2.right * speed * h);

            float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
            rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);
        }
        if ((Input.GetKey("space")) && (checktground==true))
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
        }
        if (Input.GetKeyDown(KeyCode.F) && (linternaEn == false))
        {
            Invoke("encender", 0.1f);
        }
        if (Input.GetKeyDown(KeyCode.F) && (linternaEn == true))
        {
            Invoke("apagar", 0.1f);
        }
    }
    
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Floor"))
        {
            checktground = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Floor"))
        {
            checktground = false;
        }
    }
    private void encender()
    {
        linterna.SetActive(true);
        linternaEn = true;
    }
    private void apagar()
    {
        linterna.SetActive(false);
        linternaEn = false;
    }
}
